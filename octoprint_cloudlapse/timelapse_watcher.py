from queue import Queue
from threading import RLock

from watchdog.observers import Observer

from watchdog.events import FileSystemEventHandler


class TimelapseWatcher:

	def __init__(self, dir):
		self.observer = Observer()
		self.dir = dir
		self.event_handler = Handler()

	def start(self):
		self.observer.schedule(self.event_handler, self.dir, recursive=True)
		self.observer.start()
		return

	def stop(self):
		self.observer.stop()
		self.observer.join()

	def add_listener(self, listener):
		self.event_handler.add_listener(listener)

	def remove_all_listeners(self):
		self.event_handler.remove_all_listeners()


class TimelapseListener:

	def __init__(self, on_timelapse_added, on_timelapse_removed):
		self.on_timelapse_added = on_timelapse_added
		self.on_timelapse_removed = on_timelapse_removed


class Handler(FileSystemEventHandler):

	def __init__(self):
		self.listeners = []
		self.lock = RLock()

	def add_listener(self, listener):
		self.lock.acquire()
		try:
			self.listeners.append(listener)
		finally:
			self.lock.release()

	def remove_all_listeners(self):
		self.lock.acquire()
		try:
			self.listeners.clear()
		finally:
			self.lock.release()

	def on_any_event(self, event):
		if event.is_directory:
			return None

		elif event.event_type == 'created':
			# Take any action here when a file is first created.
			print("Received created event - %s.".format(event.src_path))
			for listener in self.listeners:
				listener.on_timelapse_added(event.src_path)

		elif event.event_type == 'modified':
			# Taken any action here when a file is modified.
			print("Received modified event - %s.".format(event.src_path))
