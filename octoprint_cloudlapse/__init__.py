# coding=utf-8
from __future__ import absolute_import

import octoprint.plugin

from octoprint_cloudlapse.storage.storage_provider import StorageProviderFactory
from octoprint_cloudlapse.timelapse_watcher import TimelapseWatcher


class CloudlapsePlugin(octoprint.plugin.StartupPlugin,
					   octoprint.plugin.ShutdownPlugin,
					   octoprint.plugin.SettingsPlugin):

	def __init__(self):
		super().__init__()
		self.timelapse_watcher = None


	def get_settings_defaults(self):
		return {
			"timelapseDir": ".octoprint/timelapse/",
			"providers": []
		}
	##~~ Softwareupdate hook

	def get_update_information(self):
		# Define the configuration for your plugin to use with the Software Update
		# Plugin here. See https://github.com/foosel/OctoPrint/wiki/Plugin:-Software-Update
		# for details.
		return dict(
			cloudlapse=dict(
				displayName="Cloudlapse Plugin",
				displayVersion=self._plugin_version,

				# version check: github repository
				type="github_release",
				user="mcarlin",
				repo="OctoPrint-Cloudlapse",
				current=self._plugin_version,

				# update method: pip
				pip="https://github.com/mcarlin/OctoPrint-Cloudlapse/archive/{target_version}.zip"
			)
		)

	def on_after_startup(self):
		self.timelapse_watcher = TimelapseWatcher(self._settings.get(["timelapseDir"]))

		for provider_config in self._settings.get(["providers"]):
			provider = StorageProviderFactory.create(provider_config['type'], provider_config['settings'])
			timelapse_watcher.add_listener()

		self.timelapse_watcher.start()

	def on_shutdown(self):
		self.timelapse_watcher.stop()


__plugin_name__ = "Cloudlapse Plugin"
__plugin_pythoncompat__ = ">=3,<4" # only python 3

def __plugin_load__():
	global __plugin_implementation__
	__plugin_implementation__ = CloudlapsePlugin()

	global __plugin_hooks__
	__plugin_hooks__ = {
		"octoprint.plugin.softwareupdate.check_config": __plugin_implementation__.get_update_information
	}

