import os

from botocore.exceptions import ClientError
from octoprint import logging

from octoprint_cloudlapse.storage.storage_provider import StorageProvider

import boto3


class S3StorageProvider(StorageProvider):

	def __init__(self, settings):
		self.client = boto3.client(
			's3',
			aws_access_key_id=settings['accessKey'],
			aws_secret_access_key=settings['secretKey']
		)
		self.bucket = settings['bucket']
		self.path = settings['path']

		pass

	def upload(self, file):
		try:
			self.client.upload_file(file, self.bucket, os.path.basename(file))
		except ClientError as e:
			logging.error(e)
			return False
		return True
		pass
