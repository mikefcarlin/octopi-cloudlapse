import abc
import six

from octoprint_cloudlapse.storage.aws_storage_provider import S3StorageProvider


@six.add_metaclass(abc.ABCMeta)
class StorageProvider:

	def upload(self, file):
		raise NotImplementedError()


class StorageProviderFactory:

	@staticmethod
	def create(provider_type, settings):
		if provider_type == "aws":
			return S3StorageProvider(settings)
